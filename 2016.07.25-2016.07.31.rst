2016/07/25 - 2016/07/31. Week №30.
----------------------------------

Broken DNF's GPG support in Fedora due to new gnupg2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It became really critical because *gnupg2-2.1.13* went to stable for `F24 <https://bodhi.fedoraproject.org/updates/FEDORA-2016-b3d13b330a>`__ although I did *-1* to karma in Bodhi and already `broken <https://bugzilla.redhat.com/show_bug.cgi?id=1359521>`__ in rawhide almost for a week.

I realized that *pygpgme* spec file needs some love, so I created `Review Request: python-pygpgme <https://bugzilla.redhat.com/show_bug.cgi?id=1358357>`_ which fixes a lot of issues and backports patches from Debian to fix issues with recent *gnupg2*, but it was still failing. Then I bisected gnupg2 and found commit which breaks *pygpgme*, backported patch for *gpgme* from upstream and wrote patch to fix *pygpgme*, built all these things in F23+ and submitted updates.

And finally wrote `post <https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/message/5DAXFPMJEIISHLKNCYTGYMDLBW2F5GKK/>`__ to devel@ and test@ mailing list with describing issue and solution.

DNF functional testing more friendly
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
*ci-dnf-stack* should not be only our own repository, it should be core part of DNF components so everyone can download repository, write test, try it and propose for inclusion.

Simple tool to build container with tests and to run those tests
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
I `added <https://github.com/rpm-software-management/ci-dnf-stack/commit/3f48f758d46f335fe76ba0e36ed68f2e10a5981b>`__ tool which can list tests, build container with them and run one or more tests which will make life easier for existing contributors and will help to get more contributors.

Testing "Obsoletes" and how they behave
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Started gathering use-cases, find guidelines how people are using *Obsoletes* tag in RPM. And sent `some initial test-cases <https://github.com/rpm-software-management/ci-dnf-stack/pull/174>`__ for *ci-dnf-stack*.

Broken langpacks in updates and updates-testing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Looks like that in base (*fedora*) repo it just works as repo metadata contains information about weak and rich dependencies, but it `doesn't work <https://bugzilla.redhat.com/show_bug.cgi?id=1319073>`__ for *updates* and *updates-testing* repositories. I `reported <https://github.com/fedora-infra/bodhi/issues/875>`__ issue about it to *bodhi* repository, so this is not DNF's fault, it's about Fedora Infrastructure.

.. code-block:: shell

   $ sudo dnf -q --disablerepo=\* --enablerepo=fedora repoquery --supplements glibc-langpack-cs
   (glibc = 2.23.1-7.fc24 and (langpacks-cs or langpacks-cs_CZ))
   $ sudo dnf -q --disablerepo=\* --enablerepo=updates repoquery --supplements glibc-langpack-cs
   $

libdhif -> libdnf
^^^^^^^^^^^^^^^^^
Finally fixed last bits in `libdnf work <https://github.com/rpm-software-management/libhif/commit/2f3fdd78f3d1274555223f3cdc8542dc16d28d9c>`__, `few bits <https://github.com/rpm-software-management/ci-dnf-stack/commit/245beafa573013ff9f3dbe9b0d7a966afe623e8f>`__ in *ci-dnf-stack* and merged renaming. Once more tested that everything still works.

More standardized way of "disabling non-supported plugins" in RPM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It's not secret that plugins in RPM use ``RPMRC_NOTFOUND`` return code in ``_init()`` to indicate that they can't work on current system. For example, systemd-inhibit plugin: if ``/run/systemd/system/`` is not directory, then return ``RPMRC_NOTFOUND`` as this plugin doesn't support current system (e.g. systemd is not running). In docker all RPM transactions were spamming message: *error: Plugin systemd_inhibit: hook init failed* which was messing up our CI output a bit and providing bad User Experience. So I `proposed <https://github.com/rpm-software-management/rpm/pull/75>`__ to make this return code to be just signal for disabling plugin and do not print any errors into terminal.

Rust and Cargo in Fedora
^^^^^^^^^^^^^^^^^^^^^^^^
Recently Mozilla decided to `use Rust starting with Firefox 48 <https://hacks.mozilla.org/2016/07/shipping-rust-in-firefox/>`__.

    Rust is a systems programming language that runs blazingly fast, prevents segfaults, and guarantees thread safety. 
    -- https://www.rust-lang.org

Moreover, it's one of Changes for F25: `Changes/RustCompiler <https://fedoraproject.org/wiki/Changes/RustCompiler>`_.

Review Request: rust - The Rust Programming Language
""""""""""""""""""""""""""""""""""""""""""""""""""""
`Review Request: rust <https://bugzilla.redhat.com/show_bug.cgi?id=1356907>`_ created by `Josh Stone <https://github.com/cuviper>`_ some time ago and I did only some quick notes at that time, but now I made full review and approved package for including into Fedora. Though it requires asking FESCo for bootstraping exception and some other things, but first milestone is done.

Review Request: cargo - Rust's package manager and build tool
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
`Review Request: cargo <https://bugzilla.redhat.com/show_bug.cgi?id=1357749>`_ also made by `Josh Stone`_. I did some initial review, full review comes next week.

Maintenance of packages
^^^^^^^^^^^^^^^^^^^^^^^

libsolv 0.6.23 for F24, F25 and F26
"""""""""""""""""""""""""""""""""""
* Stupidly simple `commit <http://pkgs.fedoraproject.org/cgit/rpms/libsolv.git/commit/?id=10bb86621eb56163dab6ab5d2750abdd3945bc8f>`__ which removes backported patches, updates version and archive.
* Update for `F24 <https://bodhi.fedoraproject.org/updates/FEDORA-2016-7ea4bf4663>`__
* Build for `F25 <http://koji.fedoraproject.org/koji/buildinfo?buildID=785874>`__
* Build for `F26 <http://koji.fedoraproject.org/koji/buildinfo?buildID=785873>`__

DNF crashes with --debugsolver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When I tried to debug with nightly builds why dnf doesn't install latest version without ``--best`` I found out that ``--debugsolver`` segfaults, but didn't pay attention. After *0.6.23* update in Fedora, `Chris Murphy <https://www.linkedin.com/in/chris-murphy-12734a10>`_ reported `bug <https://bugzilla.redhat.com/show_bug.cgi?id=1361831>`__ about same thing, so I started working on it. I `bisected <https://bugzilla.redhat.com/show_bug.cgi?id=1361831#c19>`__ it and proposed `Pull Request <https://github.com/openSUSE/libsolv/pull/151>`__ to fix bug. If `Michael Schröder <https://github.com/mlschroe>`_ will accept fix, I will build patched version of libsolv in Fedora ASAP.

